#!/bin/bash

yalc publish --push --scripts --sig
rm -r ~/work/finfam/rcp/node_modules/.vite/  # trigger HMR
touch ~/work/finfam/rcp/vite.config.ts